# Projet chauffage

Projet Arduino Nano de gestion du chauffage

---

## Sommaire des fonctionnalités

- [**A) Mesure de la rempérature**](https://gitlab.com/BenBouck/projet-chauffage#a-mesure-de-la-temp%C3%A9rature)
- [**B) Gestion de l'heure**](https://gitlab.com/BenBouck/projet-chauffage#b-gestion-de-lheure)
- [**C) Gestion marche/arrêt de la chaudière**](https://gitlab.com/BenBouck/projet-chauffage#c-gestion-marchearr%C3%AAt-de-la-chaudi%C3%A8re)
    - *C1* Sur seuil de température
    - *C2* Sur plage horaire
- [**D) Autonomie énergétique (batterie)**](https://gitlab.com/BenBouck/projet-chauffage#d-autonomie-%C3%A9nerg%C3%A9tique)
    - *D1* Gestion du niveau de charge
    - *D2* Gestion de la recharge
- [**E) Affichage**](https://gitlab.com/BenBouck/projet-chauffage#e-affichage)
    - *E1* Affichage des températures
    - *E2* Affichage de l'heure
    - *E3* Affichage de la plage actuelle
    - *E4* Affichage du niveau de batterie
        - *E4.1 Par une LED*
        - *E4.2 Sur l'écran*
- [**F) Gestion de l'écriture des consignes par l'utilisateur**](https://gitlab.com/BenBouck/projet-chauffage#f-gestion-de-l%C3%A9criture-des-consignes-par-lutilisateur)
    - *F1* Écriture des seuils jour/nuit
    - *F2* Écriture des plages horaires
    - *F3* Remise à l'heure

## Schéma global

**TODO**

---

## A) Mesure de la température

Utilisation d'une sonde PT1000 câblée en pont de Wheatstone.

### Composants

- **01** sonde PT1000
- **03** résistance 1kΩ

### E/S

|Type|Description|Quantité|
|---|---|---|
|AI|Mesure la tension pour la température|1|
|DO|Alimentation du pont|1|

### Montage

**TODO**

## B) Gestion de l'heure

Utilisation d'une RTC pour sauvegarder l'heure.

### Composants

- **01** RTC DS3231 (@1101000)
- **01** Pile CR2032

### E/S

Bus I²C

### Montage

**TODO**

## C) Gestion marche/arrêt de la chaudière

Utilisation d'un transistor 2N2222 pour commander la chaudière.

### Composants 

- **01** transistor 2N2222
- **01** résistance d'émetteur 2.7kΩ
- **01** résistance de base 200kΩ

### E/S

|Type|Description|Quantité|
|---|---|---|
|DO|Sortie pilotage transistor 2N2222|1|

### Montage

**TODO**

## D) Autonomie énergétique

### Composants

- **01** batterie 5V 3000mAh
- **01** module de charge USB type C
- **01** interrupteur de déconnexion de la batterie
- **01** regulateur de tension AMS1117

### E/S

|Type|Description|Quantité|
|---|---|---|
|AI|Mesure de la tention de la batterie|1|

### Montage 

**TODO**

## E) Affichage

Utilisation d'un affcicheur bicolore OLED I²C et d'une LED d'indication de batterie faible.

### Composants

- **01** affcicheur SSD1306 (@011110X)
- **01** LED rouge
- **01** résistance de LED 180Ω

### E/S

|Type|Description|Quantité|
|---|---|---|
|DO|LED Batterie faible|1|

Bus I²C

### Montage

**TODO**

## F) Gestion de l'écriture des consignes par l'utilisateur

Interface de boutons pour gérer la commande utilisateur.

### Composants

- **05** boutons :
    - \+
    - \-
    - forçage mode 1h
    - changement consignes
    - changement plages
- **01** codeur rotatif EC11

### E/S

|Type|Description|Quantité|
|---|---|---|
|DI|Boutons|5|
|DI|Codeur rotatif|2|

### Montage
